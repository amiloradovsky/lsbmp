The codec itself is tiny (~150 LoC), no bloat.

Compile trivially, e.g.

    gcc -Wall main.c

To test run

    ./a.out -i original.bmp -o generated.bmp -c content.dat

look at the original and generated pictures, seek for artifacts, (b)diff them, and then

    ./a.out -i generated.bmp -c extracted.dat

Compare the original content and the extracted one.

Don't forget to homogenize the content before putting it into the raw image -- compress or encrypt it.

To Do
-----

It may be easily generalized to arbitrary linear encoding of the data,
with arbitrary shift at the beginning and distance(s), magics, etc.