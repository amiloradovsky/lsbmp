#include <fcntl.h>
#include <unistd.h>
#include <err.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAGIC (u_int32_t)'BMPC'
#define BMP_HEADER_SIZE 56

enum dire { DECODE, ENCODE };

struct bmpc_header {
  u_int32_t magic;  /* 'BMPC' */
  u_int32_t size;   /* content length */
  int32_t part;     /* when negative, treasted as ^-1 */
};

int
codec(buf, data, dir, size, part, s)
     u_int8_t *buf;
     void *data;
     enum dire dir;
     int size, part, s;
{
  int i;

  if (part > 8) return -1;
  s = 0;
  for (i = 0; i < size; i++) {
    if (dir == ENCODE)
      buf[i] = (buf[i] & (-1 << part)) |
        ((*((u_int16_t*)data) >> s) & ~(-1 << part));
    else
      *((u_int16_t*)data) = (*((u_int16_t*)data) & ~(-1 << s)) |
        ((~(-1 << part) & buf[i]) << s);
    s += part;
    if (s >= 8) {
      s -= 8;
      (u_int8_t*)data++;
    }
  }
  return s;
}

int
main(int argc, char **argv)
{
  int rfd, wfd, tfd, c;
  char *in_path, *out_path, *txt_path, buf[1024], data[1024];
  struct bmpc_header hdr;
  struct stat sb;

  /* parsing and checking options and their arguments */
  in_path = out_path = txt_path = 0;
  while ((c = getopt(argc, argv, "i:o:c:")) != -1) {
    if (c == 'i')
      in_path = optarg;
    else if (c == 'o')
      out_path = optarg;
    else if (c == 'c')
      txt_path = optarg;
    else
      goto usage;
  }
  if (!in_path && !txt_path)
    goto usage;
  if ((rfd = open(in_path, O_RDONLY)) < 0)
    err(1, "opening input file");
  if (out_path &&
      (wfd = open(out_path, O_WRONLY|O_CREAT|O_TRUNC, 0644)) < 0)
    err(1, "opening output file");
  if ((tfd = open(txt_path,
		  out_path ? O_RDONLY : O_WRONLY|O_CREAT|O_TRUNC, 0644)) < 0)
    err(1, "opening content file");

  /* seek\copy .BMP header */
  if (out_path) {
    if ((read  (rfd, buf, BMP_HEADER_SIZE) != BMP_HEADER_SIZE) ||
        (write (wfd, buf, BMP_HEADER_SIZE) != BMP_HEADER_SIZE))
      err(1, "copying header");
  } else
    lseek(rfd, BMP_HEADER_SIZE, SEEK_SET);

  /* set\check content header */
  if (out_path) {
    hdr.magic = MAGIC;  /* signature */
    if (fstat(tfd, &sb))  /* content size */
      err(1, "content file size");
    hdr.size = sb.st_size;
    if (fstat(rfd, &sb))  /* bits part */
      err(1, ".bmp file size");
    hdr.part = 8 * hdr.size / (sb.st_size -
			       BMP_HEADER_SIZE - 8 * sizeof(hdr));
    if (8 * hdr.size % (sb.st_size -
			BMP_HEADER_SIZE - 8 * sizeof(hdr)))
      hdr.part++;
    if (hdr.part > 8)
      err(1, "content too big");
    if (read(rfd, buf, sizeof(hdr) * 8) != sizeof(hdr) * 8)
      err(1, "reading space for content header");
    codec(buf, &hdr, ENCODE, sizeof(hdr) * 8, 1, 0);
    if (write(wfd, buf, sizeof(hdr) * 8) != sizeof(hdr) * 8)
      err(1, "writing content header");
  } else {
    if (read(rfd, buf, sizeof(hdr) * 8) != sizeof(hdr) * 8)
      err(1, "reading content header");
    codec(buf, &hdr, DECODE, sizeof(hdr) * 8, 1, 0);
    if (hdr.magic != MAGIC)
      err(1, "this file is not encapsulated");
  }

  /* encoding\decoding the content itself */
  if (out_path) {
    int s, n;

    s = 0;
    while ((c = read(tfd, data, sizeof(buf) * hdr.part / 8)) > 0) {
      n = 8 * c / hdr.part;  /* bytes to be encoded */
      if (read(rfd, buf, n) != n)
        err(1, "reading bitmap file");
      s = codec(buf, data, ENCODE, n, hdr.part, s);
      if (write(wfd, buf, n) != n)
        err(1, "writing bitmap file");
      *(u_int8_t*)data = *((u_int8_t*)data + n);
    }
    while ((c = read(rfd, buf, sizeof(buf))) > 0)
      write(wfd, buf, c);
  } else {
    int s, n;

    s = n = 0;
    while (hdr.size && (c = read(rfd, buf, sizeof(buf))) > 0) {
      n = c * hdr.part / 8;  /* bytes were decoded */
      if (n > hdr.size)
        n = hdr.size;
      hdr.size -= n;
      s = codec(buf, data, DECODE, c, hdr.part, s);
      if (write(tfd, data, n) != n)
        err(1, "writing content file");
      *(u_int8_t*)data = *((u_int8_t*)data + n);
    }
  }
  return 0;

 usage:
  write (1,
	 "usage: bmpcover -i <input> [-o <output>] -c <content>\n", 55);
  return 1;
}
